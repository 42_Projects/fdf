NAME	= fdf
CC		= gcc
MLX_PATH= minilibx_macos
FT_PATH	= libft_light
S_DIR	= srcs/
O_DIR	= objs/
I_DIR	= -I ./includes -I ./$(MLX_PATH) -I ./$(FT_PATH)/includes
CFLAGS	= -g -Wall -Wextra -Werror $(I_DIR) $(LIBS_DIR)
FILES	= ft_free_all.c ft_scale.c ft_error.c ft_mlx.c ft_win.c ft_img.c ft_map.c ft_color.c ft_points.c ft_draw_point.c ft_draw_line.c ft_link_points.c ft_hook.c
LIBS_F	= -L./$(FT_PATH) -lft_light -L./$(MLX_PATH) -lmlx
MLX		= -framework OpenGL -framework AppKit
SRC		= $(addprefix $(S_DIR),$(FILES))
OBJS	= $(addprefix $(O_DIR),$(FILES:.c=.o))
RM		= rm -f
COUNTER	= $$(ls srcs | wc -l | cut -d' ' -f7)

all:		start $(NAME)

$(NAME): 	$(OBJS) $(O_DIR)main.o
	@echo ""
	@echo ""
	@$(CC) $(LIBS_F) $^ -o $@ $(CFLAGS) $(MLX)
	@echo "./[0;34m$(NAME)[0;38m created."
	@tput cnorm

$(O_DIR)%.o:		$(S_DIR)%.c
	@$(CC) -c $< -o $@ $(CFLAGS)
	@echo -n '.'

$(O_DIR)main.o: main.c
	@$(CC) -c $< -o $@ $(CFLAGS)
	@echo -n '.'

clean:
	@make -C $(MLX_PATH) clean
	@make -C $(FT_PATH) clean
	@$(RM) $(OBJS) $(O_DIR)main.o

fclean: 	clean
	@$(RM) $(NAME)
	@$(RM) $(MLX_PATH)/libmlx.a
	@$(RM) $(FT_PATH)/libft_light.a
	@echo "[0;1mClear."

re:			fclean all

start:
	@tput civis
	@make -C $(MLX_PATH)
	@clear
	@echo "                     [0;34m  __  __       _        _____ _ _      "
	@echo '                      |  \/  | __ _| | _____|  ___(_) | ___ '
	@echo '                      | |\/| |/ _` | |/ / _ \ |_  | | |/ _ \'
	@echo '                      | |  | | (_| |   <  __/  _| | | |  __/'
	@echo '                      |_|  |_|\__,_|_|\_\___|_|   |_|_|\___|'
	@echo ""
	@make -C $(FT_PATH)
	@echo ""
	@echo ""
	@echo "[0;38mCompiling $(NAME)"
	@echo "[0;31m["
	@tput cup 11 $$(($(COUNTER) + 2))
	@echo "][0;36m"
	@tput cup 11 1

.PHONY: re clean fclean all
