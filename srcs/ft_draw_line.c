/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 20:28:13 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/25 11:53:50 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static char		draw_line(t_point *p, t_points *p1, t_points *p2, int *err)
{
	if (!p1 || !p2)
		return (FALSE);
	p->x = p2->x - p1->x < 0 ? -(p2->x - p1->x) : p2->x - p1->x;
	p->y = p2->y - p1->y < 0 ? -(p2->y - p1->y) : p2->y - p1->y;
	*err = p->x > p->y ? p->x / 2 : -p->y / 2;
	return (TRUE);
}

static void		draw_line_calculs(int *err, t_point p, t_points *p_, t_point s)
{
	int			err_;

	err_ = *err;
	*err -= err_ > -p.x ? p.y : 0;
	p_->x += err_ > -p.x ? s.x : 0;
	*err += err_ < p.y ? p.x : 0;
	p_->y += err_ < p.y ? s.y : 0;
}

void			ft_draw_line(t_mlx *mlx, t_points *p1, t_points *p2, int h)
{
	t_point		p;
	t_points	*p_;
	t_point		s;
	int			err;
	static int	c[12] = {0xFFFF00, 0xE8F100, 0xD1E300, 0xB9D500, 0xA2C700,
		0x8BB900, 0x74AC00, 0x5D9E00, 0x469000, 0x2E8200, 0x177400, 0xFF6600};

	if (!mlx || !draw_line(&p, p1, p2, &err))
		return ;
	s = ft_point(p1->x < p2->x ? 1 : -1, p1->y < p2->y ? 1 : -1);
	if (!(p_ = ft_points(p1->x, p1->y, p1->z)))
		return ;
	while (TRUE)
	{
		(void)c;
		if (h == 0)
			ft_draw_point(mlx, p_, ft_color(0x222222));
		else
			ft_draw_point(mlx, p_, ft_color(c[ft_abs(11 * h /
				(mlx->img->map->max_h - mlx->img->map->min_h))]));
		if ((int)p_->x == (int)p2->x && (int)p_->y == (int)p2->y)
			break ;
		draw_line_calculs(&err, p, p_, s);
	}
	free(p_);
}
