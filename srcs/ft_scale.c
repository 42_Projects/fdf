/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_scale.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/05 18:02:26 by Belotte           #+#    #+#             */
/*   Updated: 2016/02/18 20:18:16 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_up_scale(t_img *img, int x, int y)
{
	int		x_;
	int		y_;

	if (!img)
		return ;
	if (img->scale + 1 > MAX_SCALE)
		return ;
	img->scale++;
	x_ = x / (img->scale * D_SCALE);
	y_ = y / (img->scale * D_SCALE);
	img->origin.x -= x_;
	img->origin.y -= y_;
}

void		ft_down_scale(t_img *img, int x, int y)
{
	int		x_;
	int		y_;

	if (!img)
		return ;
	if (img->scale - 1 < 1)
		return ;
	img->scale--;
	img->scale = img->scale < 1 ? 1 : img->scale;
	x_ = x / (img->scale * D_SCALE);
	y_ = y / (img->scale * D_SCALE);
	img->origin.x += x_;
	img->origin.y += y_;
}
