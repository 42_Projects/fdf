/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_point.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 00:09:01 by Belotte           #+#    #+#             */
/*   Updated: 2016/02/20 21:33:00 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_draw_point(t_mlx *mlx, t_points *p, t_color color)
{
	int		pos;

	if (!mlx || !mlx->img || !mlx->win || !p)
		return ;
	if (p->x < 0 || p->y < 0 ||
			p->x >= mlx->img->size.width || p->y >= mlx->img->size.height)
		return ;
	pos = (p->x * 4 + p->y * mlx->img->size_line);
	mlx->img->data[pos + 0] = color.r;
	mlx->img->data[pos + 1] = color.g;
	mlx->img->data[pos + 2] = color.b;
	mlx->img->data[pos + 3] = 1;
}
