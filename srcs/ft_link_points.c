/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_link_points.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 19:16:12 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/21 15:00:59 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			ft_link_points(t_mlx *mlx)
{
	t_points	*a;
	t_points	**p;
	t_size		size;
	int			i;

	if (!mlx || !mlx->img || !mlx->img->map || !(a = mlx->img->map->points))
		return ;
	size = mlx->img->map->size;
	p = mlx->img->map->p;
	i = -1;
	while ((p[++i] = a))
		a = a->next;
	i = -1;
	while (++i < size.width * size.height)
	{
		if (i + 1 < size.width * size.height &&
			i / size.width == (i + 1) / size.width)
			ft_draw_line(mlx, p[i]->save, p[i + 1]->save, p[i]->save->h >
				p[i + 1]->save->h ? p[i]->save->h : p[i + 1]->save->h);
		if (i + size.width < size.width * size.height)
			ft_draw_line(mlx, p[i]->save, p[i + size.width]->save,
				p[i]->save->h > p[i + size.width]->save->h ? p[i]->save->h :
					p[i + size.width]->save->h);
	}
}
