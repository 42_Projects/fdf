/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_points.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 13:29:31 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/25 22:51:14 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_points		*ft_points(int x, int y, int z)
{
	t_points	*p;

	if (!(p = (t_points *)malloc(sizeof(t_points))))
		return (NULL);
	p->x = x;
	p->y = y;
	p->z = z;
	p->h = z;
	p->save = NULL;
	p->prev = NULL;
	p->next = NULL;
	return (p);
}
