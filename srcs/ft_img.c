/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_img.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 12:31:55 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/05 21:46:05 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_img			*ft_img(t_mlx *mlx, t_map *map)
{
	t_img		*img;
	t_size		size;

	if (!map || !mlx || !(img = (t_img *)malloc(sizeof(t_img))))
		return (ft_error("Error while creating 'img'.\n"));
	size.width = ft_pow(map->size.width * MAX_SCALE * D_SCALE, 2);
	size.height = ft_pow(map->size.height * MAX_SCALE * D_SCALE, 2);
	size.width = ft_sqrt(size.width + size.height);
	size.width = size.width > MAX_SIZE ? MAX_SIZE : size.width;
	size.height = size.width;
	if (!(img->ptr = mlx_new_image(mlx->ptr, size.width, size.height)))
		return (ft_error("Error while creating 'img->ptr'.\n"));
	img->map = map;
	img->size = size;
	img->origin = ft_point(200, 200);
	img->angle = ft_points(50, 110, 40);
	img->data = (t_uchar *)mlx_get_data_addr(img->ptr, &img->bpp,
											&img->size_line, &img->endian);
	img->scale = 3;
	return (img);
}

char			ft_fill_img(t_img *img, t_color color)
{
	t_point		p;

	if (!img)
		return (FALSE);
	p.x = -1;
	while (++p.x < img->size.width)
	{
		p.y = -1;
		while (++p.y < img->size.height)
		{
			img->data[p.x * 4 + 0 + p.y * img->size_line] = color.r;
			img->data[p.x * 4 + 1 + p.y * img->size_line] = color.g;
			img->data[p.x * 4 + 2 + p.y * img->size_line] = color.b;
			img->data[p.x * 4 + 3 + p.y * img->size_line] = 1;
		}
	}
	return (TRUE);
}

char			ft_clear_img(t_img *img)
{
	t_point		p;

	if (!img)
		return (FALSE);
	p.x = -1;
	while (++p.x < img->size.width)
	{
		p.y = -1;
		while (++p.y < img->size.height)
		{
			img->data[p.x * 4 + 0 + p.y * img->size_line] = 0;
			img->data[p.x * 4 + 1 + p.y * img->size_line] = 0;
			img->data[p.x * 4 + 2 + p.y * img->size_line] = 0;
			img->data[p.x * 4 + 3 + p.y * img->size_line] = 0;
		}
	}
	return (TRUE);
}

static void		point_calcul(t_points **p, t_points *angle, int scale)
{
	float		k;
	float		x;
	float		y;
	float		z;

	if (!*p || !angle)
		return ;
	x = scale * D_SCALE;
	y = scale * D_SCALE;
	z = scale * D_SCALE;
	k = angle->z * M_PI / 180;
	x *= (*p)->x * cos(k) - (*p)->y * sin(k);
	y *= (*p)->y * sin(k) + (*p)->x * cos(k);
	k = angle->x * M_PI / 180;
	y = y * cos(k) - z * sin(k);
	z = z * sin(k) + y * cos(k);
	k = angle->y * M_PI / 180;
	z = z * cos(k) - x * sin(k);
	x = x * sin(k) + z * cos(k);
	(*p)->save->x = x;
	(*p)->save->y = y;
	(*p)->save->z = z;
}

char			ft_put_img(t_mlx *mlx)
{
	t_points	*p;

	if (!mlx || !mlx->img || !mlx->img->map)
		return (FALSE);
	p = mlx->img->map->points;
	while (p)
	{
		point_calcul(&p, mlx->img->angle, mlx->img->scale);
		p->save->x -= mlx->img->scale * (0.5) * p->h - mlx->img->origin.x;
		p->save->y -= mlx->img->scale * (0.5) * p->h - mlx->img->origin.y;
		p = p->next;
	}
	ft_link_points(mlx);
	p = mlx->img->map->points;
	while (p)
	{
//		ft_draw_point(mlx, p->save, ft_color(WHITE));
		p = p->next;
	}
	return (TRUE);
}
