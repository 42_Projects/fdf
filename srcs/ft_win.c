/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_win.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 12:05:38 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/18 17:31:49 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_win			*ft_win(t_mlx *mlx, t_size size, char *title)
{
	t_win		*win;

	title = title ? title : "Title";
	size.width = size.width > MAX_SIZE ? MAX_SIZE : size.width;
	size.height = size.height > MAX_SIZE ? MAX_SIZE : size.height;
	if (!mlx || !(win = (t_win *)malloc(sizeof(t_win))) ||
		!(win->ptr = mlx_new_window(mlx->ptr, size.width, size.height, title)))
		return (ft_error("Error while creating 'win'.\n"));
	win->size = size;
	return (win);
}
