/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_all.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/18 22:41:02 by Belotte           #+#    #+#             */
/*   Updated: 2016/02/26 11:16:26 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		free_all(t_mlx **mlx)
{
	free((*mlx)->img->data);
	free((*mlx)->img->angle);
	free((*mlx)->img->map->values);
	free((*mlx)->img->map->p);
	free((*mlx)->img->map);
	free((*mlx)->img);
	free((*mlx)->win);
	free((*mlx)->ptr);
	free((*mlx));
}

void			ft_free_all(t_mlx **mlx)
{
	int			i;
	t_points	*p;
	t_lines		*l;

	if (!(i = -1) || !*mlx || !(*mlx)->img || !(*mlx)->img->map || !(*mlx)->win)
		return ;
	l = ((*mlx)->img->map->lines);
	while (l)
	{
		l = l->next;
		free((*mlx)->img->map->values[++i]);
		free((*mlx)->img->map->lines->line);
		free((*mlx)->img->map->lines);
		(*mlx)->img->map->lines = l;
	}
	p = (*mlx)->img->map->points;
	while (p)
	{
		p = p->next;
		free((*mlx)->img->map->points->save);
		free((*mlx)->img->map->points);
		(*mlx)->img->map->points = p;
	}
	free_all(mlx);
}
