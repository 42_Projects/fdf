/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hook.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 10:31:34 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/29 17:03:35 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			ft_key_press(int key, t_mlx *mlx)
{
	if (!mlx || !mlx->img)
		return (FALSE);
	if (key == 53)
	{
		ft_free_all(&mlx);
		exit(1);
	}
	mlx->img->angle->x += key == 13 ? 2 : 0;
	mlx->img->angle->x -= key == 1 ? 2 : 0;
	mlx->img->angle->y += key == 12 ? 2 : 0;
	mlx->img->angle->y -= key == 14 ? 2 : 0;
	mlx->img->angle->z += key == 0 ? 2 : 0;
	mlx->img->angle->z -= key == 2 ? 2 : 0;
	if (key == 32)
	{
		free(mlx->img->angle);
		mlx->img->angle = ft_points(50, 110, 40);
		mlx->img->origin = ft_point(200, 200);
		mlx->img->scale = 3;
	}
	else if (key == 69 || key == 78)
		ft_mouse_down(4 + (key == 78), 1, 1, mlx);
	ft_update(mlx);
	return (0);
}

int			ft_expose_hook(t_mlx *mlx)
{
	if (!mlx)
		return (FALSE);
	ft_update(mlx);
	return (TRUE);
}

int			ft_mouse_move(int x, int y, t_mlx *mlx)
{
	t_point	vector;
	t_point	origin;

	if (!mlx || !mlx->img || mlx->img->mouse_state == 0)
		return (0);
	vector.x = x - mlx->img->last_mouse_pos.x;
	vector.y = y - mlx->img->last_mouse_pos.y;
	origin = mlx->img->origin;
	if (mlx->img->mouse_state == 1)
		mlx->img->origin = ft_point(origin.x + vector.x, origin.y + vector.y);
	mlx->img->last_mouse_pos.x += vector.x;
	mlx->img->last_mouse_pos.y += vector.y;
	ft_update(mlx);
	return (1);
}

int			ft_mouse_down(int button, int x, int y, t_mlx *mlx)
{
	if (!mlx || !mlx->img)
		return (0);
	if (y <= 0)
		return (button);
	if (button == 1)
	{
		mlx->img->mouse_state = 1;
		mlx->img->last_mouse_pos = ft_point(x, y);
	}
	if (button == 4)
		ft_up_scale(mlx->img, x, y);
	if (button == 5)
		ft_down_scale(mlx->img, x, y);
	mlx->img->scale = mlx->img->scale < 1 ? 1 : mlx->img->scale;
	mlx->img->scale = mlx->img->scale > MAX_SCALE ? MAX_SCALE : mlx->img->scale;
	ft_update(mlx);
	return (button);
}

int			ft_mouse_up(int button, int x, int y, t_mlx *mlx)
{
	if (!mlx || !mlx->img)
		return (0);
	if (button == 1 || button == 2)
		mlx->img->mouse_state = 0;
	(void)x;
	(void)y;
	return (button);
}
