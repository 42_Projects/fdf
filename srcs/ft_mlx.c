/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 13:19:06 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/22 00:48:08 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_mlx			*ft_mlx(int ac, char **av)
{
	t_mlx		*mlx;
	t_map		*map;
	char		*title;

	if (ac < 2)
		return (ft_error("You must specify a file.\n"));
	title = ft_sjoin("FdF - ", av[1]);
	if (!(map = ft_map(ac, av)) ||
		!(mlx = (t_mlx *)malloc(sizeof(t_mlx))) ||
		!(mlx->ptr = mlx_init()) ||
		!(mlx->img = ft_img(mlx, map)) ||
		!(mlx->win = ft_win(mlx, mlx->img->size, title)))
		return (ft_error("Error while creating 'mlx'.\n"));
	free(title);
	return (mlx);
}

void			ft_update(t_mlx *mlx)
{
	if (!mlx)
		return ;
	mlx_clear_window(mlx->ptr, mlx->win->ptr);
	ft_clear_img(mlx->img);
	ft_put_img(mlx);
	mlx_put_image_to_window(mlx->ptr, mlx->win->ptr, mlx->img->ptr, 0, 0);
}
