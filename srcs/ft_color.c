/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 13:23:06 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/20 21:32:40 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_color			ft_color_rgb(int r, int g, int b)
{
	t_color		color;

	color.value = r * (0x10000) + g * (0x100) + b;
	color.r = r;
	color.g = g;
	color.b = b;
	return (color);
}

t_color			ft_color(int value)
{
	t_color		color;

	color.value = value;
	color.r = value / (0x10000);
	value -= color.r * (0x10000);
	color.g = value / (0x100);
	value -= color.g * (0x100);
	color.b = value;
	return (color);
}
