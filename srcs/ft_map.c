/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/17 12:58:27 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/02 13:41:49 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_lines	*read_file(char *filename)
{
	t_lines		*lines;
	char		*line;
	int			fd;
	int			ret;

	if (!filename || ((fd = open(filename, O_RDONLY)) < 1))
		return (ft_error("Error while openning file.\n"));
	lines = NULL;
	while ((ret = ft_gnl(fd, &line)) > 0)
	{
		if (!line || !ft_add_line(&lines, line))
			return (ft_error("Error while reading file.\n"));
		lines->id++;
		if (ft_slen(line) == 0)
			return (ft_error("Stop trying to open a directory !\n"));
		ft_sdel(&line);
	}
	if (ret < 0)
		return (NULL);
	ft_sdel(&line);
	close(fd);
	if (lines && lines->id < 2)
		return (ft_error("Your map must have at least two lines.\n"));
	return (lines);
}

static int		**parse_lines(t_lines *lines)
{
	int			**values;
	char		**c_val;
	t_point		p;

	if (!lines || !(values = (int **)malloc(sizeof(int *) * (lines->id))))
		return (ft_error("Error while creating 'values'.\n"));
	p.x = -1;
	while (lines && (p.y = -1))
	{
		c_val = ft_ssplit(lines->line, ' ');
		while (c_val && c_val[++p.y])
			;
		if (!(values[++p.x] = (int *)malloc(sizeof(int) * (p.y + 1))))
			return (NULL);
		values[p.x][0] = p.y;
		if ((p.y = -1) && p.x > 0 && values[p.x - 1][0] != values[p.x][0])
			return (ft_error("Err: all lines must have same nb of values.\n"));
		while (c_val[++p.y])
			values[p.x][p.y + 1] = ft_atoi(c_val[p.y]);
		while (--p.y >= 0)
			free(c_val[p.y]);
		lines = lines->next;
		free(c_val);
	}
	return (values);
}

static t_points	*parse_points(int **values, int size)
{
	t_points	*next;
	t_points	*first;
	t_point		p;

	if ((first = NULL) && !*values)
		return (ft_error("Error: 'values' is NULL.\n"));
	p.y = -1;
	while (++p.y < size && (p.x = -1))
	{
		while (++p.x < values[p.y][0])
		{
			if (!first && ++p.x)
			{
				first = ft_points(p.x - 1, p.y, values[p.y][p.x]);
				if (!(next = first) ||
					!(first->save = ft_points(p.x - 1, p.y, values[p.y][p.x])))
					return (ft_error("Error: allocation of a point failed.\n"));
			}
			if (!(next->next = ft_points(p.x, p.y, values[p.y][p.x + 1])) ||
				!(next->next->save = ft_points(p.x, p.y, values[p.y][p.x + 1])))
				return (ft_error("Error: allocation of a point failed. (2)\n"));
			next = next->next;
		}
	}
	return (first);
}

t_map			*ft_map(int ac, char **av)
{
	t_map		*map;
	t_points	*p;

	if ((ac < 2 || !av[0]) ||
			!(map = (t_map *)malloc(sizeof(t_map))))
		return (ft_error("Error: allocation of 'map' failed.\n"));
	if (!(map->lines = read_file(av[1])) ||
			!(map->values = parse_lines(map->lines))
			|| !(map->points = parse_points(map->values, map->lines->id)))
		return (ft_error("Error while parsing map.\n"));
	map->size.height = map->lines->id;
	map->size.width = map->values[0][0];
	if (!(map->p = (t_points**)malloc(sizeof(t_points *)
			* map->lines->id * map->values[0][0])))
		return (NULL);
	p = map->points;
	map->min_h = 0;
	map->max_h = 0;
	while (p && ((map->max_h = p->h > map->max_h ? p->h : map->max_h) - 2048))
	{
		map->min_h = p->h < map->min_h ? p->h : map->min_h;
		p = p->next;
	}
	return (map->values[0][0] < 2 ?
		ft_error("Your map must have at least two points by line.\n") : map);
}
