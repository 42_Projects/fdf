/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 12:21:22 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/02 13:41:20 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			main(int ac, char **av)
{
	t_mlx	*mlx;

	mlx = ft_mlx(ac, av);
	if (!mlx)
		return (1);
	ft_puts("Press A/D:       left/right rotation\n");
	ft_puts("Press W/S:       up/bottom rotation\n");
	ft_puts("Press A/D:       vertical rotation\n");
	ft_puts("Press R/T:       decrease/increase altitude\n");
	ft_puts("Press +/-/wheel: up/down scale\n");
	ft_puts("Press U:         returns to original position/size\n");
	mlx_hook(mlx->win->ptr, 12, (1L << 0), &ft_expose_hook, mlx);
	mlx_hook(mlx->win->ptr, 2, (1L << 0), &ft_key_press, mlx);
	mlx_hook(mlx->win->ptr, 4, (1L << 2), &ft_mouse_down, mlx);
	mlx_hook(mlx->win->ptr, 5, (1L << 3) | (1L << 5), &ft_mouse_up, mlx);
	mlx_hook(mlx->win->ptr, 6, (1L << 4) | (1L << 5), &ft_mouse_move, mlx);
	mlx_loop(mlx->ptr);
	return (0);
}
