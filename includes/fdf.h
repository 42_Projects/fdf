/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 11:38:57 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 19:11:07 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <fcntl.h>
# include <math.h>
# include "mlx.h"
# include "libft_light.h"

# define RED			0x0000FF
# define GREEN			0x00FF00
# define BLUE			0xFF0000
# define WHITE			0xFFFFFF
# define BLACK			0x000000

# define MAX_SIZE		1200
# define MAX_SCALE		10
# define D_SCALE		5

typedef unsigned char	t_uchar;

typedef struct s_mlx	t_mlx;
typedef struct s_win	t_win;
typedef struct s_img	t_img;
typedef struct s_map	t_map;
typedef struct s_points	t_points;
typedef struct s_color	t_color;

struct					s_mlx
{
	void				*ptr;
	t_win				*win;
	t_img				*img;
};

struct					s_win
{
	void				*ptr;

	t_size				size;
};

struct					s_img
{
	void				*ptr;
	t_map				*map;

	t_size				size;
	t_point				origin;
	t_points			*angle;

	int					mouse_state;
	t_point				origin_mouse_pos;
	t_point				last_mouse_pos;

	t_uchar				*data;
	int					bpp;
	int					size_line;
	int					endian;

	int					scale;
};

struct					s_map
{
	int					**values;
	t_points			*points;
	t_points			**p;
	t_lines				*lines;
	t_size				size;
	int					max_h;
	int					min_h;

	struct s_map		*prev;
	struct s_map		*next;
};

struct					s_points
{
	int					x;
	int					y;
	int					z;
	int					h;

	struct s_points		*save;
	struct s_points		*prev;
	struct s_points		*next;
};

struct					s_color
{
	int					value;
	t_uchar				r;
	t_uchar				g;
	t_uchar				b;
};

t_mlx					*ft_mlx(int ac, char **av);
t_win					*ft_win(t_mlx *mlx, t_size size, char *title);
t_img					*ft_img(t_mlx *mlx, t_map *map);
t_map					*ft_map(int ac, char **av);

t_points				*ft_points(int x, int y, int z);
t_color					ft_color_rgb(int r, int g, int b);
t_color					ft_color(int value);

void					ft_update(t_mlx *mlx);

char					ft_put_img(t_mlx *mlx);
char					ft_fill_img(t_img *img, t_color color);
char					ft_clear_img(t_img *img);

void					ft_draw_point(t_mlx *mlx, t_points *p, t_color color);
void					ft_draw_line(t_mlx *mlx, t_points *p1,
							t_points *p2, int h);
void					ft_link_points(t_mlx *mlx);
void					ft_up_scale(t_img *img, int x, int y);
void					ft_down_scale(t_img *img, int x, int y);

void					*ft_error(char *s);
void					ft_free_all(t_mlx **mlx);

/*
**	HOOKS
*/
int						ft_expose_hook(t_mlx *mlx);
int						ft_key_press(int key, t_mlx *mlx);
int						ft_mouse_down(int button, int x, int y, t_mlx *mlx);
int						ft_mouse_up(int button, int x, int y, t_mlx *mlx);
int						ft_mouse_move(int x, int y, t_mlx *mlx);
int						ft_expose(t_mlx *mlx);

#endif
