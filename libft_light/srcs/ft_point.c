/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_point.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/23 10:46:06 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/23 10:47:28 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_light.h"

t_point		ft_point(int x, int y)
{
	t_point	p;

	p.x = x;
	p.y = y;
	p.z = 0;
	return (p);
}

t_point		ft_point_z(int x, int y, int z)
{
	t_point	p;

	p.x = x;
	p.y = y;
	p.z = z;
	return (p);
}
