/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_line.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 10:46:57 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/20 21:34:41 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_light.h"

t_lines		*ft_add_line(t_lines **line, char *s)
{
	t_lines	*first;
	t_lines	*new;

	if (!(new = ft_lines(s)))
		return (NULL);
	if ((first = *line))
	{
		while (first->next != NULL)
			first = first->next;
		first->next = new;
	}
	else
		*line = new;
	return (*line);
}
